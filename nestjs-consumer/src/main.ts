import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import * as dotenv from 'dotenv';

dotenv.config();

async function bootstrap() {
  console.log(process.env.RQM_URI);

  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.RMQ,
    options: {
      urls: [process.env.RQM_URI],
      queue: process.env.queue,
      queueOptions: {
        durable: false,
      },
    },
  });

  await app.listen(() => console.log('Microservice is listening'));
}
bootstrap();
