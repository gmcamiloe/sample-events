import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello() {
    return {
      name: 'Camilo gonzalez',
      age: 12,
    };
  }
}
