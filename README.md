When you do, docker-compose. You must to create the vhost and queue to
the communication. For this, is neccesary to follow the next steps

1.  Open the CLI of rabbitmq > docker exec -it rabbitmq-1 bash
2.  Enable inside of rabbitmq, the rabbitmqclient management > rabbitmq-plugins enable rabbitmq_management
3.  Go to localhost:15672 and create a queue and name it "user-messages"
4.  Now go to admin screen and create a virtual host and name it "hello"

Now is available to communicate between applications, only wait to docker to restart
the applications and verify that this are running
