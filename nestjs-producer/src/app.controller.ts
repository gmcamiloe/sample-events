import { Controller, Get, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(
    @Inject('HELLO_SERVICE') private readonly client: ClientProxy,
    private readonly appService: AppService,
  ) {}

  async onApplicationBootstrap() {
    await this.client.connect();
  }

  @Get()
  getHello() {
    this.client.emit<any>('message_printed', this.appService.getHello());
    return 'Hello World printed';
  }
}
