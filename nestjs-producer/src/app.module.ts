import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { ClientRMQProvider } from './rabbitmq-config.provider';

@Module({
  imports: [ConfigModule.forRoot(), ClientRMQProvider],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
