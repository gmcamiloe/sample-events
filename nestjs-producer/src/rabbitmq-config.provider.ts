import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientsModule, RmqOptions, Transport } from '@nestjs/microservices';

export const ClientRMQProvider = ClientsModule.registerAsync([
  {
    imports: [ConfigModule],
    name: 'HELLO_SERVICE',
    inject: [ConfigService],
    useFactory: (configService: ConfigService): RmqOptions => {
      return {
        transport: Transport.RMQ,
        options: {
          urls: [configService.get<string>('RQM_URI')],
          queue: configService.get<string>('queue'),
          queueOptions: {
            durable: false,
          },
        },
      };
    },
  },
]);
